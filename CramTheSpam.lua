CramTheSpam.Halt = false;

CramTheSpam.Versioning = {};
CramTheSpam.Versioning.LocalSavedVars = 1;
CramTheSpam.Versioning.GlobalSavedVars = 3;
CramTheSpam.Versioning.ThisServerSavedVars = 2;

function CramTheSpam.Initialize()
	--variables
	
	--global set
	if( CramTheSpam.SavedVars.Global == nil ) then
		CramTheSpam.SavedVars.Global = {};
		CramTheSpam.SavedVars.Global.Version = 0;
	end
	if( CramTheSpam.SavedVars.Global.Version ~= CramTheSpam.Versioning.GlobalSavedVars ) then
		--either update or halt here
		if( CramTheSpam.UpdateGlobalSavedVarsVersion() == false ) then
			CramTheSpam.Halt = true;
			return;
		end
	end
	
	--global to just this server
	if( CramTheSpam.SavedVars.Global.ServerSpecificVars[GameData.Account.ServerName] == nil ) then
		CramTheSpam.SavedVars.Global.ServerSpecificVars[GameData.Account.ServerName] = {};
		CramTheSpam.SavedVars.Global.ServerSpecificVars[GameData.Account.ServerName].Version = 0;
	end
	
	CramTheSpam.SavedVars.ThisServer = CramTheSpam.SavedVars.Global.ServerSpecificVars[GameData.Account.ServerName];
	
	if( CramTheSpam.SavedVars.ThisServer.Version ~= CramTheSpam.Versioning.ThisServerSavedVars ) then
		--either update or halt here
		if( CramTheSpam.UpdateThisServerSavedVarsVersion() == false ) then
			CramTheSpam.Halt = true;
			return;
		end
	end
	
	--local set
	if( CramTheSpam.SavedVars.Local == nil ) then
		CramTheSpam.SavedVars.Local = {};
		CramTheSpam.SavedVars.Local.Version = 0;
	end
	if( CramTheSpam.SavedVars.Local.Version ~= CramTheSpam.Versioning.LocalSavedVars ) then
		--either update or halt here
		if( CramTheSpam.UpdateLocalSavedVarsVersion() == false ) then
			CramTheSpam.Halt = true;
			return;
		end
	end
	
	
	--and finally the runtime set of variables	
	CramTheSpam.PendingCommands = {};
	CramTheSpam.PendingCommands.Current = nil;
	CramTheSpam.PendingCommands.Timer = 0;
	CramTheSpam.PendingCommands.DefaultTimeOut = 10;
	CramTheSpam.PendingCommands.Queue = {};
	
	CramTheSpam.RemoveOfflineSpammersTimer = 0;
	
	CramTheSpam.IgnoreListSize = 0;
	
	--I can't find a chat history in the API except possibly GameData.ChatData.history, but that doesn't seem to ever have any data
	--So I'm making my own logs
	CramTheSpam.ChatLog = {};
	CramTheSpam.ChatLog.Advice = {};
	
	--any time someone says something in advice and isn't in the existing spammer list, we add them to this runtime list. 
	--The idea being that we only want to search the potentially huge spammer list once per person.
	--They're still capable of violating the filters. But they're not already flagged and we don't need to keep checking if they are
	CramTheSpam.ClearedSpeakers = {}; 
	
	CramTheSpam.IgnoreOfflineStatus = {}; --if a spammer is anonymous, we'll unignore them. But if they say ANYTHING we'll keep them ignored for this play session
	
	CramTheSpam.RunningTime = 0;
	
	
	CramTheSpam.AddDefaultTextFilters();	
	
	RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "CramTheSpam.OnChat" );
	
	CramTheSpam.EnforceMinimumIgnoreTime();
	CramTheSpam.ScanForManualSpamIgnores();
	CramTheSpam.UnignoreOfflines();
	
	--hooks and subfunctions	
	if( LibSlash ~= nil and LibSlash.RegisterWSlashCmd ~= nil ) then
		LibSlash.RegisterWSlashCmd( "cramthespam", function( wStringParam ) CramTheSpam.GenericSlashCommand( wStringParam ) end);
		CramTheSpam.Print( L"Loaded. Use /cramthespam for a list of available commands." );
	else
		CramTheSpam.Print( L"Loaded." );
	end
end

function CramTheSpam.AddDefaultTextFilters()
	CramTheSpam.AddSpamFilter( L"Welcome to www.buywargold.com 1000g bonus  =$19.99in stock <10mins delivery & professional hanned powerlevel" );
	CramTheSpam.AddSpamFilter( L"==www.Goldicq.com==Sell the Cheapest Gold! 1000G=18.71 usd! 5 mins delivery!" );
	CramTheSpam.AddSpamFilter( L"---www.hugecheap.com--- offers 5OOG=$11.45  in stock< 5mins delivery >. lvl 1-40=$88 done in 3days and more discount." );
	CramTheSpam.AddSpamFilter( L"---www.EmsGold.com--- offers 1OOO =$16 (5OOO G + 1O% Bonus).Full stock 5 mins instant delivery .lvl 1-40 done in 3days and more discount." );
	CramTheSpam.AddSpamFilter( L"Looking for WHO gold supplier & Buyback WHO gold on all server. MSN: thePurplesphere@hotmail.com  AIM: thePurpleSphere  Gtalk: Livechatbing@gmail.com" );
	CramTheSpam.AddSpamFilter( L"Dear Friend:      www.WLY.com     =======Provide Cheapest Gold====Fast Delivery within 10mins===Full Stock Now=== Safe 100%" );
	CramTheSpam.AddSpamFilter( L"==Goldicq.us====Goldicq.us==! 1000G=18.71 USD! 5 mins delivery!" );	
end



function CramTheSpam.Shutdown()
	if( LibSlash ~= nil and LibSlash.UnregisterSlashCmd ~= nil ) then
		LibSlash.UnregisterSlashCmd( "cramthespam" );
	end
	
	UnregisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "CramTheSpam.OnChat" );
end



function CramTheSpam.UpdateGlobalSavedVarsVersion()
	if( CramTheSpam.SavedVars.Global.Version > CramTheSpam.Versioning.GlobalSavedVars ) then
		return false; --not forward compatible
	end
	
	if( CramTheSpam.SavedVars.Global.Version < 1 ) then
		CramTheSpam.SavedVars.Global.TextFilters = {};
		CramTheSpam.SavedVars.Global.ServerSpecificVars = {};
		CramTheSpam.SavedVars.Global.RunningTime = 0;
	end
	
	local currentTime = CramTheSpam.GetTime();
	
	if( CramTheSpam.SavedVars.Global.Version < 2 ) then
		--update "timestamp" from running time, to first and last seen epoch time
		for i,textFilterEntry in ipairs(CramTheSpam.SavedVars.Global.TextFilters) do
			textFilterEntry.timestamp = nil;
			textFilterEntry.firstSeen = currentTime;
			textFilterEntry.lastSeen = currentTime;			
		end
		
		--now rename the table
		local tempTable = CramTheSpam.SavedVars.Global.TextFilters;
		CramTheSpam.SavedVars.Global.TextFilters = {};
		CramTheSpam.SavedVars.Global.TextFilters.Exact = tempTable;
	end
	
	if( CramTheSpam.SavedVars.Global.Version < 3 ) then
		CramTheSpam.SavedVars.Global.Verbose = false;
		CramTheSpam.SavedVars.Global.MinimumIgnoreTime = 86400; --minimum ignore time of a day
	end

	CramTheSpam.SavedVars.Global.Version = CramTheSpam.Versioning.GlobalSavedVars;
	return true;
end

function CramTheSpam.UpdateThisServerSavedVarsVersion()
	if( CramTheSpam.SavedVars.ThisServer.Version > CramTheSpam.Versioning.ThisServerSavedVars ) then
		return false; --not forward compatible
	end
	
	if( CramTheSpam.SavedVars.ThisServer.Version < 1 ) then
		CramTheSpam.SavedVars.ThisServer.SpamIgnore = {};
		CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players = {};
		CramTheSpam.SavedVars.ThisServer.RunningTime = 0;
	end
	
	local currentTime = CramTheSpam.GetTime();
	
	if( CramTheSpam.SavedVars.ThisServer.Version < 2 ) then
		--update "timestamp" from running time, to first and last seen epoch time. Also take out the name and use it as the index assuming that lua can find it faster that way
		local tempTable = CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players;
		CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players = {};
		for i,spammerEntry in ipairs(tempTable) do
			local replacementEntry = {};
			replacementEntry.firstSeen = currentTime - (CramTheSpam.SavedVars.Global.MinimumIgnoreTime * 2);
			replacementEntry.lastSeen = currentTime - (CramTheSpam.SavedVars.Global.MinimumIgnoreTime * 2); --don't trigger the minimum ignore time for every player we've ever ignored in the past
			CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[spammerEntry.name] = replacementEntry;
		end
	end
	
	CramTheSpam.SavedVars.ThisServer.Version = CramTheSpam.Versioning.ThisServerSavedVars;
	return true;
end

function CramTheSpam.UpdateLocalSavedVarsVersion()
	if( CramTheSpam.SavedVars.Local.Version > CramTheSpam.Versioning.LocalSavedVars ) then
		return false; --not forward compatible
	end
	
	if( CramTheSpam.SavedVars.Local.Version < 1 ) then
		CramTheSpam.SavedVars.Local.RunningTime = 0;
	end
	
	CramTheSpam.SavedVars.Local.Version = CramTheSpam.Versioning.LocalSavedVars;
	return true;
end




function CramTheSpam.Print( wStringParam )
	EA_ChatWindow.Print( L"[CramTheSpam] " .. wStringParam, ChatSettings.Channels[SystemData.ChatLogFilters.SHOUT].id );
end

function CramTheSpam.DebugPrint( wStringParam )
	if( CramTheSpam.SavedVars.Global.Verbose ) then
		EA_ChatWindow.Print( L"[CramTheSpam] " .. wStringParam, ChatSettings.Channels[SystemData.ChatLogFilters.SHOUT].id );
	end
	
	d( wStringParam );
end



function CramTheSpam.GetCleanName(wsName) --standardize names
	if ( wsName == nil or wsName == L"" ) then
		return( wsName );
	end

	return( wstring.match(wsName,L"([^\^]+).*") );
end



function CramTheSpam.Execute( wStringParam )
	SendChatText(wStringParam, L"");
end



function CramTheSpam.GenericSlashCommand( wStringParam )
	local _, _, wFirstWord, wOtherParams = wstring.find( wStringParam, L"^([^ ]*) ?(.*)$" );
	
	if( wFirstWord ~= nil ) then
		wFirstWord = wstring.lower( wFirstWord ); --remove case sensitivity from the equation
	end
	
	if( wFirstWord == L"start" or wFirstWord == L"run" ) then
		CramTheSpam.Print( L"Running now..." );
		CramTheSpam.ScanForManualSpamIgnores();
		CramTheSpam.ManualHalt = false;
	elseif( wFirstWord == L"stop" or wFirstWord == L"halt" ) then
		CramTheSpam.Print( L"Halting operation. Use 'start' or 'run' to resume" );
		CramTheSpam.ManualHalt = true;
	elseif( wFirstWord == L"verbose" or wFirstWord == L"debug" ) then
		CramTheSpam.Print( L"Verbose output enabled." );
		CramTheSpam.SavedVars.Global.Verbose = true;
	elseif( wFirstWord == L"quiet" ) then
		CramTheSpam.Print( L"Quiet output enabled." );
		CramTheSpam.SavedVars.Global.Verbose = false;
	elseif( wFirstWord == L"minimumignoretime" ) then
		if( wOtherParams ~= nil and wOtherParams ~= L"" ) then
			CramTheSpam.SavedVars.Global.MinimumIgnoreTime = tonumber( wOtherParams );	
			CramTheSpam.Print( L"Minimum ignore time now set to " .. CramTheSpam.SavedVars.Global.MinimumIgnoreTime .. L" seconds." );
		else
			CramTheSpam.Print( L"Please specify a new minimum ignore time for spammers in seconds. /cramthespam minimumignoretime #####" );
		end
	elseif( wFirstWord == L"fuzzytest" ) then
		if( wOtherParams ~= nil and wOtherParams ~= L"" ) then
			local numberParam = tonumber( wOtherParams );
			if( numberParam ~= 0 and CramTheSpam.SavedVars.Global.TextFilters.Exact[numberParam] ~= nil ) then
				CramTheSpam.FuzzyFilterTest( CramTheSpam.SavedVars.Global.TextFilters.Exact[numberParam].text );
			else
				CramTheSpam.FuzzyFilterTest( wOtherParams );
			end
		end
	else
		CramTheSpam.Print( L"Available commands and aliases: start(run), stop(halt), verbose(debug), quiet, minimumignoretime" );
	end
end



function CramTheSpam.QueueCommand( wStringParam, resultsCheckFunction, resultsCheckData, timeOutSeconds, onCompletedFunction, onCompletedData )
	if( timeOutSeconds == nil ) then
		timeOutSeconds = CramTheSpam.PendingCommands.DefaultTimeOut;
	end
	
	local entryStruct = {};
	entryStruct.CommandString = wStringParam;
	entryStruct.ResultsCheckFunction = resultsCheckFunction;
	entryStruct.ResultsCheckData = resultsCheckData;
	entryStruct.TimeOutSeconds = timeOutSeconds;
	entryStruct.OnCompletedFunction = onCompletedFunction;
	entryStruct.OnCompletedData = onCompletedData;	
	
	if( CramTheSpam.PendingCommands.Current == nil ) then
		CramTheSpam.PendingCommands.Current = entryStruct;
		CramTheSpam.PendingCommands.Timer = 0;
		CramTheSpam.Execute( CramTheSpam.PendingCommands.Current.CommandString );
	else
		table.insert( CramTheSpam.PendingCommands.Queue, entryStruct );
	end
end

function CramTheSpam.QueueCommandOnce( wStringParam, resultsCheckFunction, resultsCheckData, timeOutSeconds, onCompletedFunction, onCompletedData )
	--only queue this command if it's not already queued
	
	for i,queuedCommand in ipairs(CramTheSpam.PendingCommands.Queue) do
		if( queuedCommand.CommandString == wStringParam ) then --already queued
			d( L"Dropping redundant queued command: " .. wStringParam );
			return;
		end
	end
	
	--not already added, queue it
	CramTheSpam.QueueCommand( wStringParam, resultsCheckFunction, resultsCheckData, timeOutSeconds, onCompletedFunction, onCompletedData )
end



function CramTheSpam.CheckPendingCommand()
	if( CramTheSpam.PendingCommands.Current ~= nil ) then
		local bCompleted = false;
		if( CramTheSpam.PendingCommands.Current.ResultsCheckFunction ~= nil ) then
			bCompleted = CramTheSpam.PendingCommands.Current.ResultsCheckFunction( CramTheSpam.PendingCommands.Current.ResultsCheckData );
						
			if( bCompleted == false and CramTheSpam.PendingCommands.Timer >= CramTheSpam.PendingCommands.Current.TimeOutSeconds ) then
				bCompleted = true; --timed out
			end
		else
			bCompleted = true;
		end
		
		if( bCompleted == true ) then
			if( CramTheSpam.PendingCommands.Current.OnCompletedFunction ~= nil ) then
				CramTheSpam.PendingCommands.Current.OnCompletedFunction( CramTheSpam.PendingCommands.Current.OnCompletedData );
			end
			
			if( #CramTheSpam.PendingCommands.Queue > 0 ) then
				CramTheSpam.PendingCommands.Current = CramTheSpam.PendingCommands.Queue[1];
				CramTheSpam.PendingCommands.Timer = 0;
				table.remove(CramTheSpam.PendingCommands.Queue, 1);
				CramTheSpam.Execute( CramTheSpam.PendingCommands.Current.CommandString );
			else
				CramTheSpam.PendingCommands.Current = nil;
			end
		end
	end
end




--seconds per number of days
--31 = 2,678,400
--30 = 2,592,000
--29 = 2,505,600
--28 = 2,419,200
--01 = 86,400

--seconds in a year
--normal: 31536000
--leap: 31622400 (normal + 86400)

--epoch timestamp at 1/1/2008:0:0 = 1199145600. Using 2008 instead of 1970 because it makes the algorithms easier assuming nobody is using this after the year 2100

MonthSeconds_Normal = { 0, 2678400, 5097600, 7776000,	10368000, 13046400, 15638400, 18316800, 20995200, 23587200,	26265600, 28857600 }
MonthSeconds_Leap = { 0, 2678400, 5184000, 7862400, 10454400, 13132800, 15724800, 18403200, 21081600, 23673600, 26352000, 28944000 }

CramTheSpam.computerTimeLastQuery = 99999999999;
CramTheSpam.cachedDateSeconds = 0;

function CramTheSpam.GetTime()
	local computerTimeThisQuery = GetComputerTime();
	
	--only do the full date computation when we first log in or roll past midnight
	if( computerTimeThisQuery < CramTheSpam.computerTimeLastQuery ) then
		local theDate = GetTodaysDate();
		local yearsSince2008 = theDate.todaysYear - 2008;
		local yearSeconds = 1199145600 + (yearsSince2008 * 31536000) + (math.floor(yearsSince2008/4) * 86400);
		local monthSeconds;
		if( yearsSince2008 % 4 == 0 ) then --this year is a leap year
			monthSeconds = MonthSeconds_Leap[theDate.todaysMonth];		
		else
			monthSeconds = MonthSeconds_Normal[theDate.todaysMonth];
		end
		
		CramTheSpam.cachedDateSeconds = yearSeconds + monthSeconds + (theDate.todaysDay * 86400);	
	end
	CramTheSpam.computerTimeLastQuery = computerTimeThisQuery;
	return CramTheSpam.cachedDateSeconds + computerTimeThisQuery;
end


function CramTheSpam.OnChat()
	if( CramTheSpam.Halt or CramTheSpam.ManualHalt ) then
		return;
	end
	
	if( GameData.ChatData.type == SystemData.ChatLogFilters.ADVICE ) then
		local cleanName = CramTheSpam.GetCleanName( GameData.ChatData.name );
				
		local logEntry = {};
		logEntry.name = GameData.ChatData.name;
		logEntry.text = GameData.ChatData.text;
		table.insert( CramTheSpam.ChatLog.Advice, 1, logEntry );
		
		while( #CramTheSpam.ChatLog.Advice > 400 ) do
			table.remove(CramTheSpam.ChatLog.Advice);
		end
	
		if( CramTheSpam.IsKnownSpammer( cleanName, true ) ) then
			CramTheSpam.DebugPrint( L"Got spam from known spammer " .. cleanName .. L", ignoring." );
			table.insert( CramTheSpam.IgnoreOfflineStatus, cleanName );
			CramTheSpam.QueueCommandOnce( L"/ignoreadd " .. cleanName, CramTheSpam.IsIgnored, cleanName );
		elseif( CramTheSpam.IsKnownSpamText( GameData.ChatData.text, true ) ) then			
			CramTheSpam.DebugPrint( L"Player " .. cleanName .. L" sent known spam, ignoring." );
			CramTheSpam.AddSpammer( cleanName );
			CramTheSpam.QueueCommandOnce( L"/ignoreadd " .. cleanName, CramTheSpam.IsIgnored, cleanName );
		else
			CramTheSpam.ClearedSpeakers[cleanName] = true;
		end
	end
end



function CramTheSpam.OnUpdate(elapsed)
	if( CramTheSpam.Halt or CramTheSpam.ManualHalt ) then
		return;
	end
	
	--I can't find any way to reliably get the current real world time. So a bunch of runtime timers will have to do for temporal tests
	CramTheSpam.RunningTime = CramTheSpam.RunningTime + elapsed;
	CramTheSpam.SavedVars.Global.RunningTime = CramTheSpam.SavedVars.Global.RunningTime + elapsed;
	CramTheSpam.SavedVars.ThisServer.RunningTime = CramTheSpam.SavedVars.ThisServer.RunningTime + elapsed;
	CramTheSpam.SavedVars.Local.RunningTime = CramTheSpam.SavedVars.Local.RunningTime + elapsed;
	
	
	CramTheSpam.PendingCommands.Timer = CramTheSpam.PendingCommands.Timer + elapsed;
	CramTheSpam.CheckPendingCommand();
	
	if( CramTheSpam.PendingCommands.Current ~= nil ) then
		return;
	end
	
	CramTheSpam.RemoveOfflineSpammersTimer = CramTheSpam.RemoveOfflineSpammersTimer + elapsed;
	if( CramTheSpam.RemoveOfflineSpammersTimer >= 60 ) then
		CramTheSpam.RemoveOfflineSpammersTimer = 0;
		CramTheSpam.UnignoreOfflines();
	end
	
	if( CramTheSpam.PendingCommands.Current ~= nil ) then
		return;
	end
	
	local ignoreList = GetIgnoreList();
	if( CramTheSpam.IgnoreListSize ~= #ignoreList ) then
		CramTheSpam.ScanForManualSpamIgnores();
		CramTheSpam.IgnoreListSize = #ignoreList;
	end
end

function CramTheSpam.IsIgnored( wStringPlayer )
	local ignoreList = GetIgnoreList();
	for i,ignoreEntry in ipairs(ignoreList) do
		if( CramTheSpam.GetCleanName( ignoreEntry.name ) == wStringPlayer ) then
			return true;
		end
	end
	
	return false;
end

function CramTheSpam.IsNotIgnored( wStringPlayer )
	return CramTheSpam.IsIgnored( wStringPlayer ) == false;
end

function CramTheSpam.IsKnownSpammer( wStringPlayer, bShouldUpdateLastSeen )
	if( CramTheSpam.ClearedSpeakers[wStringPlayer] ~= nil ) then
		return false;
	end
	
	if( CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[wStringPlayer] ~= nil ) then
		if( bShouldUpdateLastSeen ) then
			CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[wStringPlayer].lastSeen = CramTheSpam.GetTime();
		end
		return true;
	end
	
	return false;
end

function CramTheSpam.IsKnownSpamText( wStringText, bShouldUpdateLastSeen )
	for i,ignoreFilter in ipairs(CramTheSpam.SavedVars.Global.TextFilters.Exact) do
		if( wStringText == ignoreFilter.text ) then
			if( bShouldUpdateLastSeen ) then
				ignoreFilter.lastSeen = CramTheSpam.GetTime();
			end
			return true;
		end
	end
	
	return false;
end






function CramTheSpam.AddSpamFilter( wStringParam )
	for i,filterEntry in ipairs(CramTheSpam.SavedVars.Global.TextFilters.Exact) do
		if( filterEntry.text == wStringParam ) then
			return;
		end
	end
	
	CramTheSpam.Print( L"Adding spam filter: " .. wStringParam );
	
	local currentTime = CramTheSpam.GetTime();
	
	local newEntry = {}
	newEntry.text = wStringParam;
	newEntry.firstSeen = currentTime;
	newEntry.lastSeen = currentTime;
	
	table.insert( CramTheSpam.SavedVars.Global.TextFilters.Exact, newEntry ); --add it to the filter set
end



function CramTheSpam.AddSpammer( wStringPlayer )
	if( CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[wStringPlayer] ~= nil ) then
		return;
	end
	
	if( CramTheSpam.ClearedSpeakers[wStringPlayer] ~= nil ) then
		CramTheSpam.ClearedSpeakers[wStringPlayer] = nil;
	end
	
	local currentTime = CramTheSpam.GetTime();
	
	local newEntry = {};
	newEntry.firstSeen = currentTime;
	newEntry.lastSeen = currentTime;
	CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[wStringPlayer] = newEntry;
	
	table.insert( CramTheSpam.IgnoreOfflineStatus, wStringPlayer );
	
	--search our text logs and add a filter for the most recent thing they said
	for i,logEntry in ipairs(CramTheSpam.ChatLog.Advice) do
		if( logEntry.name == wStringPlayer ) then
			CramTheSpam.AddSpamFilter( logEntry.text );
			break;
		end
	end
end

function CramTheSpam.IsAnonymous( ignoreEntry )
	if( ignoreEntry.careerName == L"" ) then
		return true;
	end
	
	return false;
end

function CramTheSpam.UnignoreOfflines()
	local currentTime = CramTheSpam.GetTime();
	
	local ignoreList = GetIgnoreList();
	for i,ignoreEntry in ipairs(ignoreList) do
		if( ignoreEntry.rank == 1 ) then --all gold sellers are assumed to be rank 1
			if( ignoreEntry.zoneID == 0 ) then --only clearing offline people
				local wStringPlayer = CramTheSpam.GetCleanName( ignoreEntry.name );
				if( CramTheSpam.IsKnownSpammer( wStringPlayer, false ) ) then
					local bSkip = false;
					for i,ignoreOfflineEntry in ipairs(CramTheSpam.IgnoreOfflineStatus) do
						if( ignoreOfflineEntry == wStringPlayer ) then
							bSkip = true; --this player either goes off/online rapidly, or is anonymous
							break;
						end
					end
					
					if( bSkip ) then
						continue;
					end
					
					if( (currentTime - CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players[wStringPlayer].lastSeen) <= CramTheSpam.SavedVars.Global.MinimumIgnoreTime ) then
						continue;
					end
					
					if( ignoreEntry.careerName == L"" ) then --player is actually anonymous
						CramTheSpam.DebugPrint( L"Unignoring anonymous spammer " .. wStringPlayer .. L" because we can't tell if they're abandoned. Will reignore for the rest of the session if they say anything." );
					end
					
					CramTheSpam.QueueCommandOnce( L"/ignoreremove " .. wStringPlayer, CramTheSpam.IsNotIgnored, wStringPlayer );
				end
			end
		end
	end	
end



function CramTheSpam.ScanForManualSpamIgnores()
	local ignoreList = GetIgnoreList();
	for i,ignoreEntry in ipairs(ignoreList) do
		if( ignoreEntry.zoneID == 100 or ignoreEntry.zoneID == 106 ) then --all the gold spammers are in norsca or nordland
			if( ignoreEntry.rank == 1 ) then --all gold sellers are assumed to be rank 1. Honestly, if you ignored a rank 1 player in norsca, you probably never want to listen to them again
				CramTheSpam.AddSpammer( CramTheSpam.GetCleanName( ignoreEntry.name ) );
			end
		end
	end
end


function CramTheSpam.EnforceMinimumIgnoreTime()
	local currentTime = CramTheSpam.GetTime();
	local ignoreList = GetIgnoreList();
	
	for wStringPlayer,playerEntry in pairs(CramTheSpam.SavedVars.ThisServer.SpamIgnore.Players) do
		if( (currentTime - playerEntry.lastSeen) <= CramTheSpam.SavedVars.Global.MinimumIgnoreTime ) then
			--we should ignore this player if we aren't already ignoring them
			local bAlreadyIgnoring = false;
			for i,ignoreEntry in ipairs(ignoreList) do
				if( CramTheSpam.GetCleanName( ignoreEntry.name ) == wStringPlayer ) then --already ignored
					bAlreadyIgnoring = true;
					break;
				end
			end
			
			if( bAlreadyIgnoring == false ) then
				CramTheSpam.DebugPrint( L"Enforcing minimum ignore time for player " .. wStringPlayer );
				CramTheSpam.QueueCommandOnce( L"/ignoreadd " .. wStringPlayer, CramTheSpam.IsIgnored, wStringPlayer, 1 );
			end 
		end
	end
end

local wStringTLDSet = L"[]";

function CramTheSpam.FuzzyFilterTest( wStringParam )
	local wLowerString = wstring.lower( wStringParam );
	local _, _, WebSite = wstring.find( wLowerString, L"%.([%w]*)%.com" );

	if( WebSite ~= nil ) then
		local deNumbered = wstring.gsub( wLowerString, L"%d+", L"#" );
		deNumbered = wstring.gsub( deNumbered, L"#%.#", L"#" );
		CramTheSpam.Print( L"Matched: " .. deNumbered );
	end
	--L"GM_V%[([^%]]*)%]"
	--bool bug bug bug
end


