<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="Cram The Spam" version="1.32" date="08/18/2010" >

    <Author name="Themm (Volkmar)" email="them@clanwho.com" />
    <Description text="An assistant to rid yourself of advice spammers" />
    <VersionSettings gameVersion="1.3.6" savedVariablesVersion="1.0"/>
    
    <Dependencies>
      <Dependency name="LibSlash" optional="true"/>
    </Dependencies>

    <Files>
      <File name="CramTheSpam_Stabilizer.lua" />
      <File name="CramTheSpam.lua" />
      <File name="CramTheSpam.xml" />
    </Files>

    <SavedVariables>
      <SavedVariable name="CramTheSpam.SavedVars.Global" global="true"/>
      <SavedVariable name="CramTheSpam.SavedVars.Local"/>
    </SavedVariables>

    <OnInitialize>
      <CallFunction name="CramTheSpam.Initialize" />
    </OnInitialize>

    <OnShutdown>
      <CallFunction name="CramTheSpam.Shutdown" />
    </OnShutdown>

    <OnUpdate>
      <CallFunction name="CramTheSpam.OnUpdateProxy" />
    </OnUpdate>
  </UiMod>
</ModuleFile>
